package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public final class final_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

 
int OrderID,price;

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"errorpage.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("<title>Order placed</title>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body bgcolor=\"#CCFFFF\">\n");
      out.write("\n");
      out.write("\t<CENTER>\n");
      out.write("\t<H1> <u><a href=\"http://www.freestudentprojects.com\">Online Shopping Cart</a></u> </H1>\n");
      out.write("\t<H2>Order Details</H2></CENTER>\n");
      out.write("\t<BR>\n");
      out.write("\t<a href=\"Logout.jsp\" ><font size=\"4\"  >Click here to Logout</font></a>\n");
      out.write("\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write('\n');

	
	String user_src=(String)session.getValue("user");
   	if (user_src!= null)
   	{
		try{
	   	Connection conn;
		conn=null;
		ResultSet rs=null;
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		conn =DriverManager.getConnection("jdbc:odbc:Data","scott","tiger"); 

				
		PreparedStatement stat1=null,stat_sel=null,stat_ins=null;
		
		OrderID=(int)(10000*Math.random()+1);
		price=Integer.parseInt((String)session.getValue("bPrice"));
		int counter=0;
		counter=Integer.parseInt((String)session.getValue("TotalSel"));

		int[] bookid=new int[counter];
		int[] bk_quantity=new int[counter];
		int org_bkqty=0;
		int i=0;
		
		String ins_query="";
		String sel_qtyQuery="";
		String ins_qty="";		
		
		for(i=1;i<=counter;i++)
		{
			ResultSet rs_sql=null;
			if((String)session.getValue("chk_var"+i)!=null)
			{
				bookid[i-1]=Integer.parseInt((String)session.getValue("chk_var"+i));
			}
			
			if((String)session.getValue("bookqty"+i)!=null)
			{
				bk_quantity[i-1]=Integer.parseInt((String)session.getValue("bookqty"+i));
			}	
			
			ins_query="INSERT INTO ORDER_DETAILS VALUES(?,?,?)";
			stat1=conn.prepareStatement(ins_query);
			stat1.setInt(1,OrderID);
			stat1.setInt(2,bookid[i-1]);
			stat1.setInt(3,bk_quantity[i-1]);
			int rs_int=0;
			
			rs_int=stat1.executeUpdate();	
			sel_qtyQuery="SELECT QUANTITY FROM BOOK_DETAILS WHERE BOOKID = ?";
					

			
			stat_sel = conn.prepareStatement(sel_qtyQuery);			
			stat_sel.setInt(1,bookid[i-1]);
			rs_sql=stat_sel.executeQuery();
			if(rs_sql.next())
			{
				org_bkqty=rs_sql.getInt(1);
			}
			rs_sql.close();
			String ns_qty="UPDATE BOOK_DETAILS SET QUANTITY=? WHERE BOOKID=?";
			stat_ins=conn.prepareStatement(ns_qty);
			
			stat_ins.setInt(1,org_bkqty-bk_quantity[i-1]);
			stat_ins.setInt(2,bookid[i-1]);
			rs_int=stat_ins.executeUpdate();	
		

			}		
			}catch(Exception e){}



		
      out.write("\n");
      out.write("\t\t<form name=\"newsrc\"  method=\"POST\">\n");
      out.write("\t\t<BR><font size=\"3\"><b>\n");
      out.write("\t\tYour order has been successfully placed.\n");
      out.write("\t\t<BR><BR>Order Number is : ");
      out.print(OrderID);
      out.write("\n");
      out.write("\t\t<BR><BR>Total Amount is : ");
      out.print(price);
      out.write("\n");
      out.write("\t\t<BR><BR>Date of Order is:\n");
      out.write("\t\t");

               
		out.println((new java.util.Date()).toLocaleString());
		
      out.write("\n");
      out.write("\t\t<BR><BR>\n");
      out.write("\t        Your Order will be delivered within next 48 working hours\n");
      out.write("\n");
      out.write("\t\t\n");

	
	String card_no=request.getParameter("Card");
	Connection con= null;
	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
	con =DriverManager.getConnection("jdbc:odbc:Data","scott","tiger");
	java.util.Date now = new java.util.Date();
	DateFormat df1 = DateFormat.getDateInstance(DateFormat.SHORT);
        String s1 = df1.format(now);			
	try
         {
        String str = "insert into order_table values(?,?,?,?)";
	PreparedStatement stat= con.prepareStatement(str);

	stat.setInt(1,OrderID);
	stat.setString(2,user_src);
	stat.setInt(3,price);
	stat.setString(4,s1);
	
	int x = stat.executeUpdate();
        
        }catch(Exception e){out.println(e); }
        
	}	
	else
	{
		response.sendRedirect("Unauthorised.html");
	} 	

	
      out.write("\t\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
