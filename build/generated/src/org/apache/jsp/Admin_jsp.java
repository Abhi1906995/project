package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Admin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("  <title>Online Shopping Site for Mobiles, Fashion, Books, Electronics, Home Appliances and More</title>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n");
      out.write("  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n");
      out.write("  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">\n");
      out.write("  <link href=\"css/ss.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("<style>\n");
      out.write("footer{\n");
      out.write("        padding: 21px;\n");
      out.write("        font-size: 17px;\n");
      out.write("        font-weight: bold;\n");
      out.write("        text-align: center;\n");
      out.write("        background: #594848;\n");
      out.write("        color: white;\n");
      out.write("        font-family: Arial;\n");
      out.write("        letter-spacing: 2px;\n");
      out.write("        }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    \n");
      out.write("<center><img src=\"images/logo.gif\" height=\"120\" width=\"300\"></center>  \n");
      out.write("\n");
      out.write("<nav class=\"navbar navbar-inverse\">\n");
      out.write("  <div class=\"container-fluid\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("        <span class=\"icon-bar\"></span>\n");
      out.write("        <span class=\"icon-bar\"></span>\n");
      out.write("        <span class=\"icon-bar\"></span>                        \n");
      out.write("      </button>\n");
      out.write("       <a class=\"navbar-brand\" href=\"#\">GKart</a>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n");
      out.write("      <ul class=\"nav navbar-nav\">\n");
      out.write("        <li><a href=\"#\">Home</a></li>\n");
      out.write("        <li><a href=\"#\">Deals</a></li>\n");
      out.write("        <li class=\"dropdown\">\n");
      out.write("        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"> Manages <span class=\"caret\"></span></a>\n");
      out.write("         <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"dis.jsp\">Customer Information</a></li>\n");
      out.write("            <li><a href=\"2.jsp\">Add Products</a></li>\n");
      out.write("            <li><a href=\"daily.jsp\">Orders</a></li>\n");
      out.write("            <li><a href=\"4.jsp\">Delete Customer</a></li>\n");
      out.write("            <li><a href=\"5.jsp\">Shopping Cart</a></li>\n");
      out.write("            <li><a href=\"\">More</a></li>\n");
      out.write("         </ul>\n");
      out.write("        </li>\n");
      out.write("        <li class=\"dropdown\">\n");
      out.write("        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"> Services <span class=\"caret\"></span></a>\n");
      out.write("         <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"feedback.jsp\">Feedback</a></li>\n");
      out.write("            <li><a href=\"#\">Contact Us</a></li>\n");
      out.write("         </ul>\n");
      out.write("        </li>\n");
      out.write("       <li><a href=\"#\">Stores</a></li>\n");
      out.write("      </ul>\n");
      out.write("          <form class=\"navbar-form navbar-left\" action=\"search.jsp\">\n");
      out.write("      <div class=\"input-group\">\n");
      out.write("        <input type=\"text\" name=\"lid\"  class=\"form-control\" placeholder=\"Search\" name=\"search\">\n");
      out.write("        <div class=\"input-group-btn\">\n");
      out.write("          <button class=\"btn btn-default\" type=\"submit\">\n");
      out.write("            <i class=\"glyphicon glyphicon-search\"></i>\n");
      out.write("          </button>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </form>  \n");
      out.write("      <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("        <li class=\"dropdown\">\n");
      out.write("        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"><span class=\"glyphicon glyphicon-user\"></span> Your Account </a>\n");
      out.write("         <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"Reg.jsp\">Register</a></li>\n");
      out.write("            <li><a href=\"login.jsp\">Login</a></li>\n");
      out.write("         </ul>\n");
      out.write("        </li>\n");
      out.write("        <li><a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Cart</a></li>\n");
      out.write("      </ul>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("</nav>\n");
      out.write("\n");
      out.write("<div class=\"w3-content w3-section\" style=\"max-width:2000px\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a1.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a2.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a3.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a4.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a5.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a6.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a7.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a8.jpg\" style=\"width:100%\">\n");
      out.write("<img class=\"mySlides\" src=\"images/a9.jpg\" style=\"width:100%\">\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<script>\n");
      out.write("var myIndex = 0;\n");
      out.write("carousel();\n");
      out.write("\n");
      out.write("function carousel() {\n");
      out.write("    var i;\n");
      out.write("    var x = document.getElementsByClassName(\"mySlides\");\n");
      out.write("    for (i = 0; i < x.length; i++) {\n");
      out.write("       x[i].style.display = \"none\";  \n");
      out.write("    }\n");
      out.write("    myIndex++;\n");
      out.write("    if (myIndex > x.length) {myIndex = 1}    \n");
      out.write("    x[myIndex-1].style.display = \"block\";  \n");
      out.write("    setTimeout(carousel, 2000); // Change image every 2 seconds\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<footer class=\"container-fluid text-center\">\n");
      out.write(" All Rights Reserved \n");
      out.write("</footer>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
