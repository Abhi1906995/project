package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class product1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <title>Product Page</title>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"description\" content=\"Love Authority.\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css\">\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("        function validate()\n");
      out.write("            {\n");
      out.write("                if (document.F1.pname.value == \"-1\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please Select Your Product\");\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                if (document.F1.pprice.value == \"\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please provide product price!\");\n");
      out.write("                    document.F1.pprice.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                if (document.F1.pqty.value == \"-1\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please Select Quantity\");\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("               return true;\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("    </head>\n");
      out.write("    <body background=\"images/nature.jpg\">\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <section class=\"hero\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 col-sm-8 mx-auto\">\n");
      out.write("                        <div class=\"card border-none\">\n");
      out.write("                            <div class=\"card-body\">\n");
      out.write("                                <div class=\"mt-2 text-center\">\n");
      out.write("                                    <h2>Product Purchase</h2>\n");
      out.write("                                </div>\n");
      out.write("                                <form action=\"ijk.jsp\" name=\"F1\" onsubmit=\"return(validate())\" method=\"post\">                                              \n");
      out.write("                                    <div class=\"mt-4\">\n");
      out.write("                                        \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <select name=\"pname\" class=\"form-control\">\n");
      out.write("                                                <option value=\"-1\" selected>Select Product</option>\n");
      out.write("                                                <option value=\"Red Ball Pen\">Red Ball Pen</option>\n");
      out.write("                                                <option value=\"Fountain Pen\">Fountain Pen</option>\n");
      out.write("                                                <option value=\"Set Of Blue Ball Pen\">Set Of Blue Ball Pen</option>\n");
      out.write("                                            </select>     \n");
      out.write("                                        </div>\n");
      out.write("                                                                                               \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"pprice\" value=\"\" placeholder=\"Enter Product Price\">\n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("                                        \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <select name=\"pqty\" class=\"form-control\">\n");
      out.write("                                                <option value=\"-1\" selected>Select Quantity</option>\n");
      out.write("                                                <option value=\"1\">1</option>\n");
      out.write("                                                <option value=\"2\">2</option>\n");
      out.write("                                                <option value=\"3\">3</option>\n");
      out.write("                                            </select>     \n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("                                        <button type=\"submit\" class=\"btn btn-primary btn-block\">Cart</button>\n");
      out.write("                                        <button type=\"reset\" class=\"btn btn-primary btn-block\">Clear</button>\n");
      out.write("\n");
      out.write("                                </form>\n");
      out.write("                                <br>\n");
      out.write("                                <p class=\"text-center\">\n");
      out.write("                                    Not Registered Yet? <a href=\"Reg.jsp\"><u>Register Here</u></a>\n");
      out.write("                                </p>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
