package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class gallery_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Bootstrap Example</title>\n");
      out.write("  <meta charset=\"utf-8\">\n");
      out.write("  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n");
      out.write("  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n");
      out.write("  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>    \n");
      out.write("  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("<style>\n");
      out.write(".container{\n");
      out.write("            max-width: 960px;\n");
      out.write("            margin: auto;\n");
      out.write("            background: #f2f2f2;\n");
      out.write("            overflow: auto;\n");
      out.write("        }\n");
      out.write("        .gallery{\n");
      out.write("            margin: 5px;\n");
      out.write("            border: 1px solid #ccc;\n");
      out.write("            float: left;\n");
      out.write("            width: 300px;\n");
      out.write("        }\n");
      out.write("        .gallery img{\n");
      out.write("            width: 100%;\n");
      out.write("            height: auto;\n");
      out.write("        }\n");
      out.write("        .desc{\n");
      out.write("            padding: 15px;\n");
      out.write("            text-align: center;\n");
      out.write("        }\n");
      out.write("        footer{\n");
      out.write("        padding: 21px;\n");
      out.write("        font-size: 17px;\n");
      out.write("        font-weight: bold;\n");
      out.write("        text-align: center;\n");
      out.write("        background: #594848;\n");
      out.write("        color: white;\n");
      out.write("        font-family: Arial;\n");
      out.write("        letter-spacing: 2px;\n");
      out.write("        }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("<nav class=\"navbar navbar-inverse\">\n");
      out.write("  <div class=\"container-fluid\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("      <a class=\"navbar-brand\" href=\"#\">GKart</a>\n");
      out.write("    </div>\n");
      out.write("    <ul class=\"nav navbar-nav\">\n");
      out.write("      <li><a href=\"Home.jsp\">Home</a></li>\n");
      out.write("      <li><a href=\"#\">About</a></li>\n");
      out.write("      <li><a href=\"#\">Contact</a></li>\n");
      out.write("      <li><a href=\"#\">Top Deals</a></li>\n");
      out.write("      <li><a href=\"\">Gallery</a></li>\n");
      out.write("      <li class=\"dropdown\">\n");
      out.write("        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Category <span class=\"caret\"></span></a>\n");
      out.write("         <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"#\">Electronics</a></li>\n");
      out.write("            <li><a href=\"#\">Shoes</a></li>\n");
      out.write("            <li><a href=\"#\">Clothes</a></li>\n");
      out.write("         </ul>\n");
      out.write("      </li>\n");
      out.write("    </ul>\n");
      out.write("    <form class=\"navbar-form navbar-left\" action=\"/action_page.php\">\n");
      out.write("      <div class=\"input-group\">\n");
      out.write("        <input type=\"text\" class=\"form-control\" placeholder=\"Search\" name=\"search\">\n");
      out.write("        <div class=\"input-group-btn\">\n");
      out.write("          <button class=\"btn btn-default\" type=\"submit\">\n");
      out.write("            <i class=\"glyphicon glyphicon-search\"></i>\n");
      out.write("          </button>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </form>\n");
      out.write("        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("        <li class=\"dropdown\">\n");
      out.write("        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"><span class=\"glyphicon glyphicon-user\"></span> Your Account</a>\n");
      out.write("         <ul class=\"dropdown-menu\">\n");
      out.write("            <li><a href=\"#\">Sign Up</a></li>\n");
      out.write("            <li><a href=\"#\">Login</a></li>\n");
      out.write("            <li><a href=\"#\">Admin Login</a></li>\n");
      out.write("         </ul>\n");
      out.write("      </li>\n");
      out.write("      </ul>  \n");
      out.write("  </div>\n");
      out.write("</nav>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <div class=\"container\">\n");
      out.write("    <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a1.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a2.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a3.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a4.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a5.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a6.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    \n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a7.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a8.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("     <div class=\"gallery\">\n");
      out.write("        <img src=\"images/a9.jpg\">\n");
      out.write("        <div class=\"desc\">Add a description of the image</div>\n");
      out.write("    </div>\n");
      out.write("        \n");
      out.write("</div>    \n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <br>\n");
      out.write("    <footer>\n");
      out.write("     All Rights Reserved Web Master       \n");
      out.write("    </footer>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
