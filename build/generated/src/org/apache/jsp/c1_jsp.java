package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class c1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"errorpage.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Order Details</title>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body bgcolor=\"#CCFFFF\">\n");
      out.write("\t<CENTER>\n");
      out.write("\t<H1> <u><a href=\"http://www.freestudentprojects.com\">Online Shopping Cart</a></u> </H1>\n");
      out.write("\t</CENTER>\n");
      out.write("\t<BR>\n");
      out.write("\t<a href=\"Logout.jsp\" ><font size=\"4\"  >Click here to Logout</font></a>\n");
      out.write("\t\n");
      out.write("\t");

	if (session.getValue("user")!=null)
	{
      out.write("\n");
      out.write("\t<script language=\"Javascript\">\n");
      out.write("\tfunction validate()\n");
      out.write("\t{\n");
      out.write(" \tvar r = document.form1;\n");
      out.write("    \tvar creditcard=r.Card.value;\n");
      out.write(" \tvar year = r.Year.value;\n");
      out.write(" \tvar month = r.Month.value;\n");
      out.write("     \tvar day = r.Day.value;\n");
      out.write("   \n");
      out.write("     \tif(creditcard.length!=16)\n");
      out.write(" \t{\n");
      out.write("\t\t   alert(\"Invalid Credit Card Number\");\n");
      out.write("\t\t   r.Card.focus();\n");
      out.write("\t\t   return;\n");
      out.write("  \t}\t\t   \n");
      out.write("//-----------------------------------------------------------------------   \t\n");
      out.write("\t\n");
      out.write("\tif(day==\"Select Day\")\n");
      out.write("\t{ \n");
      out.write("\t\talert(\"Please select the day\");\n");
      out.write("\t\tr.Day.focus();\n");
      out.write("\t\treturn;\n");
      out.write("\t}  \t\n");
      out.write("//-----------------------------------------------------------------------   \t\t\n");
      out.write("\tif(month==\"Select Month\")\n");
      out.write("\t{ \n");
      out.write("\t\talert(\"Please select the month\");\n");
      out.write("\t\tr.Month.focus();\n");
      out.write("\t\treturn;\n");
      out.write("\t}\n");
      out.write("//----------------------------------------------------------------------- \t\n");
      out.write("  \tif(year.length==0)\n");
      out.write("\t{ \n");
      out.write("\t\talert(\"Please enter the year\");\n");
      out.write("\t\tr.Year.focus();\n");
      out.write("\t}\n");
      out.write("     \telse if (year.length!=4)\n");
      out.write("\t{\n");
      out.write("\t\talert(\"Please enter the year in YYYY format\");\n");
      out.write(" \t\tr.Year.focus();\n");
      out.write("\t}\n");
      out.write("//-----------------------------------------------------------------------\n");
      out.write("     \tif (( month == \"3\" || month == \"5\" || month == \"8\" || month == \"10\") && (day == \"31\"))\n");
      out.write("        {\n");
      out.write("        \talert(\"Please enter a valid date\");\n");
      out.write("                r.Day.focus();\n");
      out.write("        }\n");
      out.write("     \tif ((month == \"1\") && (day == \"30\" || day == \"31\"))\n");
      out.write("        {\n");
      out.write("                alert(\"Please enter a valid date\");\n");
      out.write("                r.Day.focus();\n");
      out.write("        }\n");
      out.write("        \n");
      out.write("        //------- To check that card date  is not less than current date----------\n");
      out.write("        var vr_day;\n");
      out.write("\tvar vr_month;\n");
      out.write("\tvar vr_year;\n");
      out.write("\tvar d=new Date();\n");
      out.write("\tvr_year=d.getFullYear();\n");
      out.write("\tvr_month=d.getMonth();\n");
      out.write("\tvr_day=d.getDate();\n");
      out.write("\n");
      out.write("\tif(parseInt(vr_year)>parseInt(document.form1.Year.value))\n");
      out.write("\t{\n");
      out.write("\t\talert(\"Year cannot be less than Current Year\");\n");
      out.write("\t\treturn false;\n");
      out.write("\t}\n");
      out.write("\telse if(parseInt(vr_year)==parseInt(document.form1.Year.value))\n");
      out.write("\t{\n");
      out.write("\t\tif(parseInt(vr_month)>parseInt(document.form1.Month.value))\n");
      out.write("\t\t{\n");
      out.write("\t\t\talert(\"Month cannot be less than Current Month\");\n");
      out.write("\t\t\treturn false;\n");
      out.write("\t\t}\n");
      out.write("\t\telse if(parseInt(vr_month)==parseInt(document.form1.Month.value))\n");
      out.write("\t\t{\n");
      out.write("\t\t\tif(parseInt(vr_day)>parseInt(document.form1.Day.value))\n");
      out.write("\t\t\t{\n");
      out.write("\t\t\t\talert(\"Day cannot be less than current Day\");\n");
      out.write("\t\t\t\treturn false;\n");
      out.write("\t\t\t}\n");
      out.write("\t\t}\n");
      out.write("\t}\n");
      out.write("\t\n");
      out.write("\t//------------------------------------------------------------------------\n");
      out.write("\tdocument.form1.submit();\n");
      out.write("\tdocument.forms[0].action=\"Final.jsp\";\n");
      out.write("\t}\n");
      out.write("\t</script>\n");
      out.write("\t");

		int counter=0;	
		counter=Integer.parseInt((String)session.getValue("TotalSel"));
		String query="";
		int[] bookid=new int[counter];
		int[] bk_price=new int[counter];
		int[] bk_quantity=new int[counter];
		
                Connection conn;
		conn=null;
		ResultSet rs;
		rs=null;
		Class.forName("com.mysql.jdbc.Driver");
		conn =DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root"); 
		
		PreparedStatement stat=null;
		int ctr=0,bookprice=0;
		int i=1;
		while(i<=counter)
		{
			if((String)session.getValue("chk_var"+i)!=null)
			{
				bookid[i-1]=Integer.parseInt((String)session.getValue("chk_var"+i));
		
				query="SELECT PRICE FROM stu WHERE BOOKID=?";
				stat=conn.prepareStatement(query);
				stat.setInt(1,bookid[i-1]);
		
				rs=stat.executeQuery();
				if(rs.next())
				{
					bk_price[i-1]=rs.getInt(1);
				}
				else
				{
					bk_price[i-1]=0;
				}
			
			}
			
			if(request.getParameter("qty"+i)!=null)
			{
				bk_quantity[i-1]=Integer.parseInt(request.getParameter("qty"+i));
				String sess_var=String.valueOf(bk_quantity[i-1]);
				session.putValue(sess_var,String.valueOf(bk_quantity[i-1]));
			}
			
			bookprice=bookprice+(bk_price[i-1]*bk_quantity[i-1]);
			i++;
		}
		session.putValue("bPrice",String.valueOf(bookprice));
	
      out.write("\n");
      out.write("\t\n");
      out.write("\t<form method=\"POST\" action=\"final.jsp\" name=\"form1\">\n");
      out.write("  \t<table width=\"70%\">\n");
      out.write("    \t<tr>\n");
      out.write("    \t\t<td colspan=4 ><b>Total Amount (in Rs.) is : ");
      out.print(bookprice);
      out.write("</b></td></tr>\n");
      out.write("    \t<tr>\n");
      out.write("    \t</tr>\n");
      out.write("    \t<tr>\n");
      out.write("    \t\t<td><font size=\"3\"><b>Credit Card Number: </b></font></td>\n");
      out.write("      \t\t<td colspan=3>\n");
      out.write("      \t<input type=\"text\" name=\"Card\" size=\"39\" maxlength=\"16\" tab=\"1\"></td>\n");
      out.write("    \t</tr>\n");
      out.write("    \t<tr>\n");
      out.write("      \t\t<td><font size=\"3\"><b>Expiry Date :     </b></font></td>\n");
      out.write("      \t\t<td>\n");
      out.write("      \t\t     \t\t\n");
      out.write("      \t\t<select size=\"1\" name=\"Day\">\n");
      out.write("          \t<option value=\"Select Day\">Select Day</option>\n");
      out.write("          \t<option value=\"1\">1</option>\n");
      out.write("          \t<option value=\"2\">2</option>\n");
      out.write("          \t<option value=\"3\">3</option>\n");
      out.write("          \t<option value=\"4\">4</option>\n");
      out.write("          \t<option value=\"5\">5</option>\n");
      out.write("          \t<option value=\"6\">6</option>\n");
      out.write("          \t<option value=\"7\">7</option>\n");
      out.write("          \t<option value=\"8\">8</option>\n");
      out.write("          \t<option value=\"9\">9</option>\n");
      out.write("          \t<option value=\"10\">10</option>\n");
      out.write("          \t<option value=\"11\">11</option>\n");
      out.write("         \t<option value=\"12\">12</option>\n");
      out.write("         \t<option value=\"13\">13</option>\n");
      out.write("         \t<option value=\"14\">14</option>\n");
      out.write("         \t<option value=\"15\">15</option>\n");
      out.write("         \t<option value=\"16\">16</option>\n");
      out.write("         \t<option value=\"17\">17</option>\n");
      out.write("         \t<option value=\"18\">18</option>\n");
      out.write("        \t<option value=\"19\">19</option>\n");
      out.write("        \t<option value=\"20\">20</option>\n");
      out.write("        \t<option value=\"21\">21</option>\n");
      out.write("        \t<option value=\"22\">22</option>\n");
      out.write("        \t<option value=\"23\">23</option>\n");
      out.write("         \t<option value=\"24\">24</option>\n");
      out.write("        \t<option value=\"25\">25</option>\n");
      out.write("          \t<option value=\"26\">26</option>\n");
      out.write("          \t<option value=\"27\">27</option>\n");
      out.write("          \t<option value=\"28\">28</option>\n");
      out.write("          \t<option value=\"29\">29</option>\n");
      out.write("          \t<option value=\"30\">30</option>\n");
      out.write("          \t<option value=\"31\">31</option>\n");
      out.write("        \t</select>\n");
      out.write("        \t</td>\n");
      out.write("        \t<td>\n");
      out.write("        \t<select size=\"1\" name=\"Month\">\n");
      out.write("          \t<option value=\"Select Month\">Select Month</option>\n");
      out.write("          \t<option value=\"0\">January</option>\n");
      out.write("          \t<option value=\"1\">February</option>\n");
      out.write("          \t<option value=\"2\">March</option>\n");
      out.write("          \t<option value=\"3\">April</option>\n");
      out.write("          \t<option value=\"4\">May</option>\n");
      out.write("          \t<option value=\"5\">June</option>\n");
      out.write("          \t<option value=\"6\">July</option>\n");
      out.write("          \t<option value=\"7\">August</option>\n");
      out.write("          \t<option value=\"8\">September</option>\n");
      out.write("          \t<option value=\"9\">October</option>\n");
      out.write("          \t<option value=\"10\">November</option>\n");
      out.write("          \t<option value=\"11\">December</option>\n");
      out.write("        \t</select>\n");
      out.write("        \t</td>\n");
      out.write("        \t<td>\n");
      out.write("        \t<font size=\"3\"><input type=\"text\" name=\"Year\" size=\"11\"></font>\n");
      out.write("      \t\t</td>\n");
      out.write("    \t</tr>\n");
      out.write("  \t</table>\n");
      out.write("  \n");
      out.write("  \t</font><input type=\"button\" value=\"Submit\" name=\"B1\" onClick=\"validate();\" >\n");
      out.write("  \t</form>\n");
      out.write("\t");

	}
	else
	{
		response.sendRedirect("unauthorised.html");
	}
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
