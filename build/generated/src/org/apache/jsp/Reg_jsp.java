package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Reg_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <title>Registration Page</title>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"description\" content=\"Love Authority.\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css\">\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("        function validate()\n");
      out.write("            {\n");
      out.write("                if (document.Form.fname.value == \"\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please provide your Full Name!\");\n");
      out.write("                    document.Form.fname.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                var password = document.Form.password.value;\n");
      out.write("                var cpassword = document.Form.cpassword.value;\n");
      out.write("                if (password == null || password == \"\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Password can't be blank\");\n");
      out.write("                    return false;                    \n");
      out.write("                } else if (password.length < 8)\n");
      out.write("                {\n");
      out.write("                    alert(\"Password must be at least 8 characters long.\");\n");
      out.write("                    return false;\n");
      out.write("                } else if (password != cpassword)\n");
      out.write("                {\n");
      out.write("                    alert(\"password mismatch\");\n");
      out.write("                    return false;\n");
      out.write("                } else\n");
      out.write("                {\n");
      out.write("                    alert(\"Password are similar\");\n");
      out.write("                }\n");
      out.write("                if (document.Form.address.value == \"\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please provide your Address\");\n");
      out.write("                    document.Form.address.focus();\n");
      out.write("                    return false;\n");
      out.write("                }                \n");
      out.write("                if (document.Form.Gender.value == \"-1\")\n");
      out.write("                {\n");
      out.write("                    alert(\"Please Select Your Gender\");\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                if (document.Form.pincode.value == \"\" ||\n");
      out.write("                        isNaN(document.Form.pincode.value) ||\n");
      out.write("                        document.Form.pincode.value.length != 6)\n");
      out.write("                {\n");
      out.write("                    alert(\"Please provide a pincode in the format ######.\");\n");
      out.write("                    document.Form.pincode.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                if (document.Form.Contact.value == \"\" ||\n");
      out.write("                        isNaN(document.Form.Contact.value) ||\n");
      out.write("                        document.Form.Contact.value.length != 10)\n");
      out.write("                {\n");
      out.write("                    alert(\"Please provide a Mobil No in the format #####.\");\n");
      out.write("                    document.Form.Contact.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                var emailID = document.Form.email.value;\n");
      out.write("                atpos = emailID.indexOf(\"@\");\n");
      out.write("                dotpos = emailID.lastIndexOf(\".\");\n");
      out.write("                if (atpos < 1 || (dotpos - atpos < 2))\n");
      out.write("                {\n");
      out.write("                    alert(\"Please enter correct email ID\")\n");
      out.write("                    document.Form.email.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                return(true);\n");
      out.write("            }\n");
      out.write("            function isNumberKey(evt)\n");
      out.write("            {\n");
      out.write("                var charCode = (evt.which) ? evt.which : event.keyCode;\n");
      out.write("                if (charCode != 46 && charCode > 31 &&\n");
      out.write("                        (charCode < 48 || charCode > 57))\n");
      out.write("                {\n");
      out.write("                    alert(\"Enter Number\");\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("                return true;\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("    </head>\n");
      out.write("    <body background=\"images/nature.jpg\">\n");
      out.write("        <p>\n");
      out.write("        <section class=\"hero\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 col-sm-8 mx-auto\">\n");
      out.write("                        <div class=\"card border-none\">\n");
      out.write("                            <div class=\"card-body\">\n");
      out.write("                                <div class=\"mt-2 text-center\">\n");
      out.write("                                    <h2>Register Now</h2>\n");
      out.write("                                </div>\n");
      out.write("                                <form action=\"insert.jsp\" name=\"Form\" onsubmit=\"return(validate())\" method=\"post\">                                              \n");
      out.write("                                    <div class=\"mt-4\">\n");
      out.write("                                    \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"fname\" value=\"\" placeholder=\"Full Name\">\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"password\" class=\"form-control\" name=\"password\" value=\"\" placeholder=\"Create Password\">\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"password\" class=\"form-control\" name=\"cpassword\" value=\"\" placeholder=\"Confirm your password\">\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"address\" value=\"\" placeholder=\"Address\">\n");
      out.write("                                        </div>\n");
      out.write("                     \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <select name=\"Gender\" class=\"form-control\">\n");
      out.write("                                                <option value=\"-1\" selected>Select Gender</option>\n");
      out.write("                                                <option value=\"Male\">Male</option>\n");
      out.write("                                                <option value=\"Female\">Female</option>\n");
      out.write("                                            </select>     \n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"pincode\" value=\"\" placeholder=\"Pincode\">\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"form-group\"> \n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"Contact\" value=\"\" placeholder=\"Contact number\">\n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <input type=\"email\" class=\"form-control\" name=\"email\" value=\"\" placeholder=\"Email Id\">\n");
      out.write("                                        </div>\n");
      out.write("                                                                           \n");
      out.write("                                        <button type=\"submit\" class=\"btn btn-primary btn-block\">Create Account</button>\n");
      out.write("                                </form>\n");
      out.write("                                <br>\n");
      out.write("                                <p class=\"text-center\">\n");
      out.write("                                    Already have an account? <a href=\"login.jsp\"><u>Login Now</u></a>\n");
      out.write("                                </p>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
