package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class _1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("function validate()\n");
      out.write("var x;\n");
      out.write("x=document.f1.username.Value;\n");
      out.write("if(x.Value.length<=2 !isNaN(x.Value))\n");
      out.write("{\n");
      out.write("alert(\"Name is not valid\");\n");
      out.write("x.Value=\" \":\n");
      out.write("//x.setFocus();\n");
      out.write("return false;\n");
      out.write("}\n");
      out.write("var fchar=x.Value.charAt(0);\n");
      out.write("if(fchar=='0' || fchar==\"1\" || fchar==\"2\" || fchar==\"3\" || fchar==\"4\" || fchar==\"5\" || fchar==\"6\" || fchar==\"7\" || fchar==\"8\" || fchar==\"9\")\n");
      out.write("{\n");
      out.write("alert(\"fchar cannot be a number\");\n");
      out.write("x.Value=\" \":\n");
      out.write("//x.setFocus();\n");
      out.write("return false;\n");
      out.write("}\n");
      out.write("else\n");
      out.write("{\n");
      out.write("return(true);\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("    <body>\n");
      out.write("    <form action=\"\" method=\"post\" name=\"f1\" onSubmit=\"return validate();\">\n");
      out.write("Name:<input type=\"text\" name=\"username\">\n");
      out.write("<br>\n");
      out.write("<input type=\"submit\" value=\"Submit\">\n");
      out.write("</form>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
