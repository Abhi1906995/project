package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("<!--\n");
      out.write(" Licensed to the Apache Software Foundation (ASF) under one or more\n");
      out.write("  contributor license agreements.  See the NOTICE file distributed with\n");
      out.write("  this work for additional information regarding copyright ownership.\n");
      out.write("  The ASF licenses this file to You under the Apache License, Version 2.0\n");
      out.write("  (the \"License\"); you may not use this file except in compliance with\n");
      out.write("  the License.  You may obtain a copy of the License at\n");
      out.write("\n");
      out.write("      http://www.apache.org/licenses/LICENSE-2.0\n");
      out.write("\n");
      out.write("  Unless required by applicable law or agreed to in writing, software\n");
      out.write("  distributed under the License is distributed on an \"AS IS\" BASIS,\n");
      out.write("  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n");
      out.write("  See the License for the specific language governing permissions and\n");
      out.write("  limitations under the License.\n");
      out.write("-->\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <title>carts</title>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write(" <body bgcolor=\"white\">\n");
      out.write("<font size = 5 color=\"#CC0000\">\n");
      out.write("\n");
      out.write("<form type=POST action=carts.jsp>\n");
      out.write("<BR>\n");
      out.write("Please enter item to add or remove:\n");
      out.write("<br>\n");
      out.write("Add Item:\n");
      out.write("\n");
      out.write("<SELECT NAME=\"item\">\n");
      out.write("<OPTION>Beavis & Butt-head Video collection\n");
      out.write("<OPTION>X-files movie\n");
      out.write("<OPTION>Twin peaks tapes\n");
      out.write("<OPTION>NIN CD\n");
      out.write("<OPTION>JSP Book\n");
      out.write("<OPTION>Concert tickets\n");
      out.write("<OPTION>Love life\n");
      out.write("<OPTION>Switch blade\n");
      out.write("<OPTION>Rex, Rugs & Rock n' Roll\n");
      out.write("</SELECT>\n");
      out.write("\n");
      out.write("\n");
      out.write("<br> <br>\n");
      out.write("<INPUT TYPE=submit name=\"submit\" value=\"add\">\n");
      out.write("<INPUT TYPE=submit name=\"submit\" value=\"remove\">\n");
      out.write("\n");
      out.write("</form>\n");
      out.write("       \n");
      out.write("</FONT>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
