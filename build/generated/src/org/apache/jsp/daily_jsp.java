package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class daily_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html >\n");
      out.write("<head>\n");
      out.write("  <meta charset=\"UTF-8\">\n");
      out.write("  <title>Daily UI #012: E-Commerce Shop (Single Item)</title>\n");
      out.write("  \n");
      out.write("  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">\n");
      out.write("\n");
      out.write("  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'>\n");
      out.write("<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css'>\n");
      out.write("<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css'>\n");
      out.write("\n");
      out.write("      <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("\n");
      out.write("  \n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("  <!--\n");
      out.write("\n");
      out.write("Follow me on\n");
      out.write("Dribbble: https://dribbble.com/supahfunk\n");
      out.write("Twitter: https://twitter.com/supahfunk\n");
      out.write("Codepen: http://codepen.io/supah/\n");
      out.write("\n");
      out.write("-->\n");
      out.write("<div class=\"shop-card\">\n");
      out.write("  <div class=\"title\">\n");
      out.write("    Nike Metcon 2\n");
      out.write("  </div>\n");
      out.write("  <div class=\"desc\">\n");
      out.write("    Men's training shoe\n");
      out.write("  </div>\n");
      out.write("  <div class=\"slider\">\n");
      out.write("    <figure data-color=\"#E24938, #A30F22\">\n");
      out.write("      <img src=\"http://www.supah.it/dribbble/012/1.jpg\" />\n");
      out.write("    </figure>\n");
      out.write("    <figure data-color=\"#6CD96A, #00986F\">\n");
      out.write("      <img src=\"http://www.supah.it/dribbble/012/2.jpg\" />\n");
      out.write("    </figure>\n");
      out.write("    <figure data-color=\"#4795D1, #006EB8\">\n");
      out.write("      <img src=\"http://www.supah.it/dribbble/012/3.jpg\" />\n");
      out.write("    </figure>\n");
      out.write("    <figure data-color=\"#292a2f, #131519\">\n");
      out.write("      <img src=\"http://www.supah.it/dribbble/012/4.jpg\" />\n");
      out.write("    </figure>\n");
      out.write("  </div>\n");
      out.write("\n");
      out.write("  <div class=\"cta\">\n");
      out.write("    <div class=\"price\">$130</div>\n");
      out.write("    <button class=\"btn\">Add to cart<span class=\"bg\"></span></button>\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("<div class=\"bg\"></div>\n");
      out.write("  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>\n");
      out.write("<script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js '></script>\n");
      out.write("\n");
      out.write("    <script src=\"js/index.js\"></script>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
