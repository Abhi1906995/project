package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class cart_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Shopping Cart</title>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<form>\n");
      out.write("<center>\n");
      out.write("<table border=\"!\">\n");
      out.write("<thead>\n");
      out.write("<tr>\n");
      out.write("<th colspan=\"12\">Shopping Cart</th>\n");
      out.write("</tr>\n");
      out.write("</thead>\n");
      out.write("<tbody>\n");
      out.write("<tr>\n");
      out.write("<td>Product Id</td>\n");
      out.write("<td>Product Name</td>\n");
      out.write("<td>Product Image</td>\n");
      out.write("<td>Product Price</td>\n");
      out.write("<td>Order Status</td>\n");
      out.write("<td>Order No</td>\n");
      out.write("<td>Product Description</td>\n");
      out.write("<td>Product Quantity</td>\n");
      out.write("<td>Total Price</td>\n");
      out.write("<td>Update Price</td>\n");
      out.write("<td>Update Quantity</td>\n");
      out.write("<td>Actions</td>\n");
      out.write("</tr>\n");
      out.write("<td>1</td>\n");
      out.write("<td>3 Idiots</td>\n");
      out.write("<td><img src=\"4.jpg\"></td>\n");
      out.write("<td>2:00 hrs</td>\n");
      out.write("<td>hd</td>\n");
      out.write("<td>hsijo</td>\n");
      out.write("<td>It is a Indian comedy-drama film directed and written by Rajkumar Hirani.</td>\n");
      out.write("<td>Hindi</td>\n");
      out.write("<td>gjhksj</td>\n");
      out.write("<td>Update Price</td>\n");
      out.write("<td>Update Quantity</td>\n");
      out.write("<td><a href=\"update.jsp\"><button type=\"button\" class=\"update\">Update</button></a><br>\n");
      out.write("<a href=\"delete.jsp\"><button type=\"button\" class=\"delete\">Delete</button></a></td>\n");
      out.write("</tr>\n");
      out.write("</tbody>\n");
      out.write("</table>\n");
      out.write("</center>\n");
      out.write("</form>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
