<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<html lang="en">
    <head>
        <title>Edit Changes</title>
        <meta charset="utf-8">
        <meta name="description" content="Love Authority." />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<%
String id = request.getParameter("userid");
String driver = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String database = "test";
String userid = "root";
String password = "root";
try {
Class.forName(driver);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<%
try{
connection = DriverManager.getConnection(connectionUrl+database, userid, password);
statement=connection.createStatement();
String sql ="select * from users where userid="+id;
resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<!DOCTYPE html>
<html>
<body background="images/nature.jpg">
<form method="post" action="update-process.jsp">
    <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2 text-center">
                                    <h2>Edit Changes</h2>
                                </div>
                                   <div class="mt-4">
                                    
                                        <div class="form-group">
                                        <input type="hidden" name="userid" value="<%=resultSet.getInt("userid") %>">
                                        <input type="text" name="userid" value="<%=resultSet.getInt("userid") %>">
                                        </div>                                        

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="fname" value="<%=resultSet.getString("fname") %>" placeholder="Full Name">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" value="<%=resultSet.getString("password") %>" placeholder="Password">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="address" value="<%=resultSet.getString("address") %>" placeholder="Address">
                                        </div>

                                        <div class="form-group">
                                            <select name="Gender" class="form-control" value="<%=resultSet.getString("Gender") %>">
                                                <option value="-1" selected>Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>     
                                        </div>
                         
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="pincode" value="" placeholder="Pincode">
                                        </div>
        
                                        <div class="form-group"> 
                                            <input type="text" class="form-control" name="Contact" value="<%=resultSet.getString("Contact") %>" placeholder="Contact number">
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" value="<%=resultSet.getString("email") %>" placeholder="Email Id">
                                        </div>
                                     
                                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                                <br>
                             </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
</form>                                           
<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>
</body>
</html>