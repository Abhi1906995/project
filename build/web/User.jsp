<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <title>Online Shopping Site for Mobiles, Fashion, Books, Electronics, Home Appliances and More</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link href="css/ss.css" rel="stylesheet" type="text/css"/>
<style>
footer{
        padding: 21px;
        font-size: 17px;
        font-weight: bold;
        text-align: center;
        background: #594848;
        color: white;
        font-family: Arial;
        letter-spacing: 2px;
        }
</style>
</head>
<body>
<center><img src="images/logo.gif" height="120" width="300"></center>  

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
       <a class="navbar-brand" href="#">GKart</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#">Home</a></li>
        <li><a href="#">Deals</a></li>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Departments <span class="caret"></span></a>
         <ul class="dropdown-menu">
            <li><a href="fpen.jsp">Fountain Pen</a></li>
            <li><a href="rbp.jsp">Red Ball Pen</a></li>
            <li><a href="bbp.jsp">Set Of Blue Ball Pens</a></li>
         </ul>
        </li>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Services <span class="caret"></span></a>
         <ul class="dropdown-menu">
            <li><a href="feedback.jsp">Feedback</a></li>
            <li><a href="contact.jsp">Contact Us</a></li>
         </ul>
        </li>
       <li><a href="#">Stores</a></li>
      </ul>
          <form class="navbar-form navbar-left" action="search.jsp">
      <div class="input-group">
        <input type="text" name="lid" class="form-control" placeholder="Search" name="search">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
    </form>  
      <ul class="nav navbar-nav navbar-right">
         <li><a href=""><font color="red"><% String fname = request.getParameter("fname");
         out.println("Welcome "+fname); %></font></a></li>
         <li><a href="login.jsp">Logout</a></li>      
         <li><a href="cart.jsp"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="w3-content w3-section" style="max-width:2000px">
<img class="mySlides" src="images/a1.jpg" style="width:100%">
<img class="mySlides" src="images/a2.jpg" style="width:100%">
<img class="mySlides" src="images/a3.jpg" style="width:100%">
<img class="mySlides" src="images/a4.jpg" style="width:100%">
<img class="mySlides" src="images/a5.jpg" style="width:100%">
<img class="mySlides" src="images/a6.jpg" style="width:100%">
<img class="mySlides" src="images/a7.jpg" style="width:100%">
<img class="mySlides" src="images/a8.jpg" style="width:100%">
<img class="mySlides" src="images/a9.jpg" style="width:100%">
</div>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>
<br>
<footer class="container-fluid text-center">
 All Rights Reserved 
</footer>

</body>
</html>