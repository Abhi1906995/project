<!DOCTYPE html>
<html>
<title>Contact Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
function validate()
{
                    if( document.Feedback.fname.value == "" )
                    {
                        alert( "Please provide your Fullname!" );
                        document.Feedback.fname.focus() ;
                        return false;
                    }
                    var email = document.Feedback.email.value;
                    atpos = email.indexOf("@");
                    dotpos = email.lastIndexOf(".");
                    if (email == "" || atpos < 1 || (dotpos - atpos < 2))
                    {
                        alert("Please enter correct email ID")
                        document.Feedback.email.focus();
                        return false;
                    }
                    if (document.Feedback.Contact.value == "" ||
                            isNaN(document.Feedback.Contact.value) ||
                            document.Feedback.Contact.value.length != 10)
                    {
                        alert("Please provide a Mobil No in the format #####.");
                        document.Feedback.Contact.focus();
                        return false;
                    }
                    if (document.Feedback.message.value == "")
                    {
                        alert("Please provide your Problem!");
                        document.Feedback.message.focus();
                        return false;
                    }
                    return(true);
                }
                function isNumberKey(evt)
                {
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (charCode != 46 && charCode > 31 &&
                            (charCode < 48 || charCode > 57))
                    {
                        alert("Enter Number");
                        return false;
                    }
                    return true;
                }
</script>
</head>
<body>

    <form action="ins.jsp" class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin" onsubmit="return(validate());">
<h2 class="w3-center">Contact Us</h2>
 
<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="fname" type="text" placeholder="Full Name">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="email" type="text" placeholder="Email">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="Contact" type="text" placeholder="Contact Number">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-pencil"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="message" type="text" placeholder="Message">
    </div>
</div>

<button class="w3-button w3-block w3-section w3-blue w3-ripple w3-padding">Send</button>

</form>

</body>
</html> 