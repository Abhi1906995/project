<html lang="en">
    <head>
        <title>Registration Page</title>
        <meta charset="utf-8">
        <meta name="description" content="Love Authority." />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <script type="text/javascript">
        function validate()
            {
                if (document.Form.fname.value == "")
                {
                    alert("Please provide your Full Name!");
                    document.Form.fname.focus();
                    return false;
                }
                var password = document.Form.password.value;
                var cpassword = document.Form.cpassword.value;
                if (password == null || password == "")
                {
                    alert("Password can't be blank");
                    return false;                    
                } else if (password.length < 8)
                {
                    alert("Password must be at least 8 characters long.");
                    return false;
                } else if (password != cpassword)
                {
                    alert("password mismatch");
                    return false;
                } else
                {
                    alert("Password are similar");
                }
                if (document.Form.address.value == "")
                {
                    alert("Please provide your Address");
                    document.Form.address.focus();
                    return false;
                }                
                if (document.Form.Gender.value == "-1")
                {
                    alert("Please Select Your Gender");
                    return false;
                }
                if (document.Form.pincode.value == "" ||
                        isNaN(document.Form.pincode.value) ||
                        document.Form.pincode.value.length != 6)
                {
                    alert("Please provide a pincode in the format ######.");
                    document.Form.pincode.focus();
                    return false;
                }
                if (document.Form.Contact.value == "" ||
                        isNaN(document.Form.Contact.value) ||
                        document.Form.Contact.value.length != 10)
                {
                    alert("Please provide a Mobil No in the format #####.");
                    document.Form.Contact.focus();
                    return false;
                }
                var emailID = document.Form.email.value;
                atpos = emailID.indexOf("@");
                dotpos = emailID.lastIndexOf(".");
                if (atpos < 1 || (dotpos - atpos < 2))
                {
                    alert("Please enter correct email ID")
                    document.Form.email.focus();
                    return false;
                }
                return(true);
            }
            function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 46 && charCode > 31 &&
                        (charCode < 48 || charCode > 57))
                {
                    alert("Enter Number");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body background="images/nature.jpg">
        <p>
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2 text-center">
                                    <h2>Register Now</h2>
                                </div>
                                <form action="insert.jsp" name="Form" onsubmit="return(validate())" method="post">                                              
                                    <div class="mt-4">
                                    
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="fname" value="" placeholder="Full Name">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" value="" placeholder="Create Password">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" name="cpassword" value="" placeholder="Confirm your password">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="address" value="" placeholder="Address">
                                        </div>
                     
                                        <div class="form-group">
                                            <select name="Gender" class="form-control">
                                                <option value="-1" selected>Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>     
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="pincode" value="" placeholder="Pincode">
                                        </div>

                                        <div class="form-group"> 
                                            <input type="text" class="form-control" name="Contact" value="" placeholder="Contact number">
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" value="" placeholder="Email Id">
                                        </div>
                                                                           
                                        <button type="submit" class="btn btn-primary btn-block">Create Account</button>
                                </form>
                                <br>
                                <p class="text-center">
                                    Already have an account? <a href="login.jsp"><u>Login Now</u></a>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </body>
</html>