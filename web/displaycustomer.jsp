<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Details</title>
    </head>
    <body>
<%
String userid = request.getParameter("userid");
String driverName = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String dbName = "test";
String userId = "root";
String password = "root";

try {
Class.forName(driverName);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<h2 align="center"><font><strong>Customer Details</strong></font></h2>
<table align="center" cellpadding="5" cellspacing="5" border="1">
<tr>

</tr>
<tr>
<td><b>UserId</b></td>
<td><b>Full Name</b></td>
<td><b>Password</b></td>
<td><b>Address</b></td>
<td><b>Gender</b></td>
<td><b>Pincode</b></td>
<td><b>Contact</b></td>
<td><b>Email</b></td>
<td><b>Actions</b></td>
</tr>
<%
try{ 
connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
statement=connection.createStatement();
String sql ="SELECT * FROM users";

resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr>

<td><%=resultSet.getInt("userid") %></td>
<td><%=resultSet.getString("fname") %></td>
<td><%=resultSet.getString("password") %></td>
<td><%=resultSet.getString("address") %></td>
<td><%=resultSet.getString("Gender") %></td>
<td><%=resultSet.getInt("pincode") %></td>
<td><%=resultSet.getString("Contact") %></td>
<td><%=resultSet.getString("email") %></td>
<td><a href="delete.jsp?userid=<%=resultSet.getInt("userid") %>"><button type="button" class="delete">Delete</button></a></td></tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
</tr>
</table>
</body>
</html>