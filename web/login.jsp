<html lang="en">
    <head>
        <title>Login Page</title>
        <meta charset="utf-8">
        <meta name="description" content="Love Authority." />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <script type="text/javascript">
        function validate()
            {
                if (document.Form.fname.value == "")
                {
                    alert("Please provide your fullname!");
                    document.Form.fname.focus();
                    return false;
                }
                var password = document.Form.password.value;
                if (password == null || password == "")
                {
                    alert("Password can't be blank");
                    return false;                    
                } 
                else if (password.length < 8)
                {
                    alert("Password must be at least 8 characters long.");
                    return false;
                }
               return true;
            }
        </script>
    </head>
    <body background="images/nature.jpg">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h4><% session.invalidate(); %></h4>
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2 text-center">
                                    <h2>Login Now</h2>
                                </div>
                                <form action="lgn.jsp" name="Form" onsubmit="return(validate())" method="post">                                              
                                    <div class="mt-4">
                                        
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="fname" value="" placeholder="Full Name">
                                        </div>

                                       
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" value="" placeholder="Password">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                                        <button type="reset" class="btn btn-primary btn-block">Clear</button>

                                </form>
                                <br>
                                <p class="text-center">
                                    Not Registered Yet? <a href="Reg.jsp"><u>Register Here</u></a>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </body>
</html>