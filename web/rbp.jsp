<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Daily Products</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
    <form action="checkout.jsp">
  <!--
Follow me on
Dribbble: https://dribbble.com/supahfunk
Twitter: https://twitter.com/supahfunk
Codepen: http://codepen.io/supah/

-->
<div class="shop-card">
  <div class="title">
  Red Ball Pen
  </div>
  <div class="slider">
    <figure data-color="#6CD96A, #00986F">
      <img src="images/P1.png" />
    </figure>
   </div>

  <div class="cta">
    <div class="price">$1</div>
    <button class="btn">Add to cart<span class="bg"></span></button>
  </div>
</div>
<div class="bg"></div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js '></script>

    <script src="js/index.js"></script>
    </form>
</body>
</html>